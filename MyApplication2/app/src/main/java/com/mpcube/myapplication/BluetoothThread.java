package com.mpcube.myapplication;

import java.util.concurrent.ConcurrentLinkedQueue;

public class BluetoothThread extends Thread{
    private static BluetoothThread instance;
    public static synchronized BluetoothThread getInstance(){
        if(instance == null){
            instance = new BluetoothThread();
            instance.start();
        }
        return instance;
    }

    private ConcurrentLinkedQueue<Runnable> taches;

    public BluetoothThread(){
        this.taches = new ConcurrentLinkedQueue<>();
    }

    @Override
    public void run(){
        while(true){
            if(this.taches.isEmpty()){
                continue;
            }
            for(Runnable runnable : this.taches){
                 runnable.run();
            }
            this.taches.clear();
        }
    }

    public void ajouterTache(Runnable tache){
        this.taches.add(tache);
    }
}
